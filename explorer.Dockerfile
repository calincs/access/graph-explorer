FROM node:18 AS build
WORKDIR /usr/src/app
RUN npm install -g pnpm vite
COPY . .
WORKDIR /usr/src/app/packages/graph-explorer
ENV HOME=/usr/src/app/packages/graph-explorer
RUN npm install
RUN npm run build

FROM node:18
RUN npm install -g serve
USER node
WORKDIR /usr/src/app
# COPY --chown=node:node --from=build /usr/src/app/packages/graph-explorer/node_modules ./node_modules
# COPY --chown=node:node --from=build /usr/src/app/packages/graph-explorer/public ./public
# COPY --chown=node:node --from=build /usr/src/app/packages/graph-explorer/package*.json .
COPY --chown=node:node --from=build /usr/src/app/packages/graph-explorer/dist ./dist
EXPOSE 3000
CMD [ "serve", "-s", "dist" ]
